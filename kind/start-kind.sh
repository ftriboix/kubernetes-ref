#/bin/bash

set -euo pipefail

# Create the kind cluster
kind create cluster --config kind-config.yaml

# Create the NGINX ingress
sleep 10
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml

# Wait for the NGINX ingress to be up and running
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=90s
