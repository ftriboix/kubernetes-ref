What is this?
=============

This repo demonstrates a toy project (essentially an NGINX web server)
deployed as a statefulset within a Kubernetes cluster, with an
accompanying ingress. This is done for a number of Kubernetes cluster
solutions.

In any case, you must have
[kubectl](https://github.com/kubernetes/kubectl) and
[Helm](https://helm.sh/docs/intro/install/) installed.

Minikube
========

Minikube is a tool to run a Kubernetes locally, typically in a
VirtualBox VM.

For this [sub-project](minikube), you need to have
[minikube](https://minikube.sigs.k8s.io/docs/)
[installed](https://github.com/kubernetes/minikube) and configured as
well as Docker.

Just run the following:

```sh
$ cd minikube
$ ./start-minikube.sh
$ helm install myapp myapp
$ ip=$(minikube ip)
$ echo "$ip myapp.local" | sudo tee -a /etc/hosts  # or edit /etc/hosts directly
$ curl myapp.local
```

Kind
====

[Kind](https://kind.sigs.k8s.io/) is another tool to run a Kubernetes
locally, using only Docker (kind stands for "Kubernetes in docker").
This [sub-project](kind) obviously requires both Docker and kind to be
installed locally.

Just run the following:

```sh
$ cd kind
$ ./start-kind.sh
$ helm install myapp myapp
$ ip=$(docker container inspect kind-control-plane --format '{{ .NetworkSettings.Networks.kind.IPAddress }}')
$ echo "$ip myapp.local" | sudo tee -a /etc/hosts  # or edit /etc/hosts directly
$ curl myapp.local
```

EKS
===
